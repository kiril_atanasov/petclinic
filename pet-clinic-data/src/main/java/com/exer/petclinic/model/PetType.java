package com.exer.petclinic.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "types")
@Getter
@Setter
public class PetType extends BaseEntity {

    public static final String UNSPECIFIED = "unspecified";
    @Column(name = "name")
    private String name;

    @Builder
    public PetType(Long id, String name) {
        super(id);
        this.name = name;
    }

    public PetType() {

    }

    @Override
    public String toString() {
        return name;
    }
}
