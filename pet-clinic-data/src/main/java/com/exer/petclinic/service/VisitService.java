package com.exer.petclinic.service;

import com.exer.petclinic.model.Visit;

public interface VisitService extends CrudService<Visit, Long> {
}
