package com.exer.petclinic.service;

import com.exer.petclinic.model.Vet;


public interface VetService extends CrudService<Vet, Long> {
}
