package com.exer.petclinic.service.map;

import com.exer.petclinic.model.Specialty;
import com.exer.petclinic.model.Vet;
import com.exer.petclinic.service.SpecialityService;
import com.exer.petclinic.service.VetService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Profile({"map"})
public class VetServiceMap extends AbstractMapService<Vet, Long>
        implements VetService {

    private final SpecialityService specialityService;

    public VetServiceMap(SpecialityService specialityService) {
        this.specialityService = specialityService;
    }

    @Override
    public Set<Vet> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Vet object) {
        super.delete(object);
    }

    @Override
    public Vet findById(Long id) {
        return this.findById(id);
    }

    @Override
    public Vet save(Vet vet) {
        if (vet.getSpecialties().size() > 0) {
            vet.getSpecialties().forEach(
                    specialty -> {
                        if (specialty.getId() == null) {
                            Specialty savedSpecialty = specialityService.save(specialty);
                            specialty.setId(savedSpecialty.getId());
                        }
                    }
            );
        }

        return super.save(vet);
    }
}
