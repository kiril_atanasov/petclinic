package com.exer.petclinic.service;

import com.exer.petclinic.model.PetType;

public interface PetTypeService extends CrudService<PetType, Long> {
}
