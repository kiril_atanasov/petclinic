package com.exer.petclinic.service;

import com.exer.petclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long> {

}
