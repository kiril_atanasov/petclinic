package com.exer.petclinic.service;

import com.exer.petclinic.model.Specialty;

public interface SpecialityService extends CrudService<Specialty, Long> {
}
