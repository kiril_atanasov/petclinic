package com.exer.petclinic.service.map;

import com.exer.petclinic.model.Owner;
import com.exer.petclinic.service.OwnerService;
import com.exer.petclinic.service.PetService;
import com.exer.petclinic.service.PetTypeService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Profile({"map"})
public class OwnerServiceMap extends AbstractMapService<Owner, Long>
        implements OwnerService {

    private final PetTypeService petTypeService;
    private final PetService petService;

    public OwnerServiceMap(PetTypeService petTypeService, PetService petService) {
        this.petTypeService = petTypeService;
        this.petService = petService;
    }

    @Override
    public Set<Owner> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Owner object) {
        super.delete(object);
    }

    @Override
    public Owner findById(Long aLong) {
        return super.findById(aLong);
    }

    @Override
    public Owner save(Owner object) {
        if (object != null) {
            if (object.getPets() != null && !object.getPets().isEmpty()) {
                object.getPets().stream().forEach(
                        pet -> {
                            if (pet.getPetType() != null) {
                                pet.setPetType(petTypeService.save(pet.getPetType()));

                                if (pet.getId() == null) {
                                    petService.save(pet);
                                }
                            } else {
                                throw new RuntimeException("PetType is required");
                            }
                        }
                );
            }
            return super.save(object);
        } else {
            return null;
        }
    }

    @Override
    public Owner findByLastName(String lastName) {
        return super.map.values().stream()
                .filter(owner -> owner.getLastName().equals(lastName))
                .findAny()
                .get();
    }

    @Override
    public List<Owner> findAllByLastNameLike(String lastName) {
        //Todo implement
        return null;
    }
}
