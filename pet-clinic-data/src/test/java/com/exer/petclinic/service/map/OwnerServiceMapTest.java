package com.exer.petclinic.service.map;

import com.exer.petclinic.model.Owner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OwnerServiceMapTest {

    private static final Long OWNER_ID = 1L;
    private static final String OWNER_LAST_NAME = "Smith";
    OwnerServiceMap ownerServiceMap;

    @BeforeEach
    void setUp() {
        ownerServiceMap = new OwnerServiceMap(new PetTypeServiceMap(), new PetServiceMap());
        Owner owner = new Owner();
        owner.setId(OWNER_ID);
        owner.setLastName(OWNER_LAST_NAME);
        ownerServiceMap.save(owner);
    }

    @Test
    void findAll() {
        assertEquals(1, ownerServiceMap.findAll().size());
    }

    @Test
    void deleteById() {
        ownerServiceMap.deleteById(OWNER_ID);
        assertNull(ownerServiceMap.findById(OWNER_ID));
    }

    @Test
    void delete() {
        Owner owner = ownerServiceMap.findById(OWNER_ID);
        ownerServiceMap.delete(owner);
        assertNull(ownerServiceMap.findById(OWNER_ID));
    }

    @Test
    void findById() {
        Owner owner = ownerServiceMap.findById(OWNER_ID);
        assertNotNull(owner);
        assertEquals(1, owner.getId());
    }

    @Test
    void save() {
        Long newOwnerId = 2L;
        Owner newOwner = new Owner();
        newOwner.setId(newOwnerId);
        ownerServiceMap.save(newOwner);

        assertEquals(newOwnerId, ownerServiceMap.findById(newOwnerId).getId());
    }

    @Test
    void findByLastName() {
        Owner owner = ownerServiceMap.findByLastName(OWNER_LAST_NAME);
        assertNotNull(owner);
        assertEquals(OWNER_ID, owner.getId());
    }
}