package com.exer.petclinic.springdatajpa;

import com.exer.petclinic.model.Owner;
import com.exer.petclinic.repositories.OwnerRepository;
import com.exer.petclinic.repositories.PetRepository;
import com.exer.petclinic.repositories.PetTypeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static java.util.Optional.empty;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OwnerSDJpaServiceTest {

    private Owner smith;
    private static final String SMITH_LAST_NAME = "Smith";
    private static final Long SMITH_ID = 1L;

    @Mock
    OwnerRepository ownerRepository;

    @Mock
    PetRepository petRepository;

    @Mock
    PetTypeRepository petTypeRepository;

    @InjectMocks
    OwnerSDJpaService ownerService;

    @BeforeEach
    void setUp() {
        smith = Owner.builder()
                .id(SMITH_ID)
                .lastName(SMITH_LAST_NAME)
                .build();
    }

    @Test
    void findByLastName() {
        when(ownerRepository.findByLastName(any())).thenReturn(smith);

        Owner owner = ownerService.findByLastName(SMITH_LAST_NAME);

        assertEquals(SMITH_LAST_NAME, owner.getLastName());

        verify(ownerRepository).findByLastName(any());
    }

    @Test
    void findAll() {
        Owner owner1 = Owner.builder()
                .id(2L)
                .lastName(SMITH_LAST_NAME)
                .build();
        Owner owner2 = Owner.builder()
                .id(3L)
                .lastName(SMITH_LAST_NAME)
                .build();

        Set<Owner> owners = new HashSet<>();
        owners.add(owner1);
        owners.add(owner2);

        when(ownerRepository.findAll()).thenReturn(owners);

        Set<Owner> result = ownerService.findAll();
        assertEquals(2, result.size());
    }

    @Test
    void findById() {
        when(ownerRepository.findById(any())).thenReturn(java.util.Optional.of(smith));

        Owner owner = ownerService.findById(SMITH_ID);
        assertEquals(SMITH_LAST_NAME, owner.getLastName());

        verify(ownerRepository).findById(any());
    }

    @Test
    void findById_NotFound() {
        when(ownerRepository.findById(any())).thenReturn(empty());
        Owner owner = ownerService.findById(5L);
        assertNull(owner);
    }

    @Test
    void save() {
        when(ownerRepository.save(any())).thenReturn(smith);
        Owner savedOwner = ownerService.save(smith);
        assertNotNull(savedOwner);
    }

    @Test
    void delete() {
        ownerService.delete(smith);
        verify(ownerRepository).delete(any());
    }

    @Test
    void deleteById() {
        ownerService.deleteById(SMITH_ID);

        verify(ownerRepository).deleteById(anyLong());
    }
}