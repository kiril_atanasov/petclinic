package com.exer.petclinic.controller;

import com.exer.petclinic.model.Owner;
import com.exer.petclinic.model.Pet;
import com.exer.petclinic.model.PetType;
import com.exer.petclinic.model.Visit;
import com.exer.petclinic.service.OwnerService;
import com.exer.petclinic.service.PetService;
import com.exer.petclinic.service.VisitService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class VisitControllerTest {

    private static final Long TEST_PET_ID = 1L;
    private static final Long TEST_OWNER_ID = 1L;
    private static final Long TEST_VISIT_ID = 1L;
    public static final String PETS_CREATE_OR_UPDATE_VISIT_FORM = "pets/createOrUpdateVisitForm";
    public static final String REDIRECT_OWNER_ID = "redirect:/owners/{ownerId}";

    private final Owner owner = Owner.builder().id(TEST_OWNER_ID)
                        .firstName("John")
                        .lastName("Smith")
                        .build();

    private final Pet pet = Pet.builder().id(TEST_PET_ID)
            .name("Rodger")
            .owner(owner)
            .birthDate(LocalDate.of(2016, 1, 1))
            .petType(PetType.builder().id(1L).name("Cat").build())
            .build();
    private final Visit visit = Visit.builder().id(TEST_VISIT_ID).pet(pet).build();

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    VisitController visitController;

    @Mock
    private VisitService visitService;

    @Mock
    private PetService petService;

    @Mock
    private OwnerService ownerService;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(visitController).build();
    }

    @Test
    void testInitNewVisitForm() throws Exception {
        when(petService.findById(anyLong())).thenReturn(pet);

        mockMvc.perform(get("/owners/*/pets/{petId}/visits/new", TEST_PET_ID))
                .andExpect(status().isOk())
                .andExpect(view().name("pets/createOrUpdateVisitForm"));
    }

    @Test
    void testProcessNewVisitFormSuccess() throws Exception {
        when(petService.findById(anyLong())).thenReturn(pet);

        mockMvc.perform(post("/owners/{ownerId}/pets/{petId}/visits/new",TEST_OWNER_ID, TEST_PET_ID)
                .param("name", "George")
                .param("description", "Visit Description"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name(REDIRECT_OWNER_ID));

        verify(visitService).save(any());
    }
}