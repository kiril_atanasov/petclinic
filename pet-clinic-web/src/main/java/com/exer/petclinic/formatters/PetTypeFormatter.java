package com.exer.petclinic.formatters;

import com.exer.petclinic.model.PetType;
import com.exer.petclinic.service.PetTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Collection;
import java.util.Locale;

@Component
public class PetTypeFormatter implements Formatter<PetType> {

    @Autowired
    private PetTypeService petTypeService;

    @Override
    public PetType parse(String text, Locale locale) throws ParseException {
        Collection<PetType> petTypes = petTypeService.findAll();
        for (PetType petType : petTypes) {
            if (text.equals(petType.getName())) {
                return petType;
            }
        }

        throw new ParseException("type not found: " + text, 0);
    }

    @Override
    public String print(PetType petType, Locale locale) {
        return petType.toString();
    }
}
