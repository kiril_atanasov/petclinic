package com.exer.petclinic.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping({"", "/", "index.html"})
    public String index(Model model) {
        // no need to add, because of internationalization
//        model.addAttribute("welcome", "WELCOME!!");
        return "index";
    }

    @RequestMapping("/oups")
    public String oups() {
        return "notimplemented";
    }

}
