package com.exer.petclinic.bootstrap;

import com.exer.petclinic.model.*;
import com.exer.petclinic.service.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DataInitializer implements CommandLineRunner {

    private final OwnerService ownerService;
    private final VetService vetService;
    private final PetTypeService petTypeService;
    private final SpecialityService specialityService;
    private final VisitService visitService;


    public DataInitializer(OwnerService ownerService, VetService vetService, PetTypeService petTypeService, SpecialityService specialityService, VisitService visitService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
        this.petTypeService = petTypeService;
        this.specialityService = specialityService;
        this.visitService = visitService;
    }

    @Override
    public void run(String... args) throws Exception {

        Specialty dentistry = new Specialty();
//        dentistry.
        dentistry.setDescription("Dentistry");
        specialityService.save(dentistry);

        Specialty surgery = new Specialty();
        surgery.setDescription("Surgery");
        specialityService.save(surgery);

        Specialty radiolology = new Specialty();
        radiolology.setDescription("Radiolology");
        specialityService.save(radiolology);

        System.out.println("Loaded Specialties");

        PetType dog = new PetType();
        dog.setName("dog");
        petTypeService.save(dog);

        PetType cat = new PetType();
        cat.setName("cat");
        petTypeService.save(cat);
        System.out.println("Loaded Pet Types");

        Owner owner1 = new Owner();
        owner1.setFirstName("Anton");
        owner1.setLastName("Antonov");
        owner1.setAddress("Maria Luiza 10");
        owner1.setCity("Sofia");
        owner1.setTelephone("123123123");

        Pet p1 = new Pet();
        p1.setPetType(dog);
        p1.setOwner(owner1);
        p1.setName("Rodger");
        p1.setPetType(dog);
        p1.setBirthDate(LocalDate.now());

        Pet p2 = new Pet();
        p2.setPetType(dog);
        p2.setOwner(owner1);
        p2.setName("Dodger");
        p2.setBirthDate(LocalDate.now());

        owner1.getPets().add(p1);
        owner1.getPets().add(p2);

        ownerService.save(owner1);

        Owner owner2 = new Owner();
        owner2.setFirstName("Blagovest");
        owner2.setLastName("Bojidarov");
        owner2.setAddress("Aleksandrovska 7");
        owner2.setCity("Burgas");
        owner2.setTelephone("123123124");

        Pet p3 = new Pet();
        p3.setPetType(cat);
        p3.setOwner(owner2);
        p3.setName("Fiona");
        p3.setBirthDate(LocalDate.now());

        owner2.getPets().add(p3);

        ownerService.save(owner2);

        System.out.println("Loaded Owners");

        Vet vet1 = new Vet();
        vet1.setFirstName("Zlatin");
        vet1.setLastName("Zlatev");
        vet1.getSpecialties().add(dentistry);
        vet1.getSpecialties().add(surgery);
        vetService.save(vet1);

        Vet vet2 = new Vet();
        vet2.setFirstName("Marin");
        vet2.setLastName("Martev");
        vet2.getSpecialties().add(radiolology);
        vetService.save(vet2);

        Visit visit1 = new Visit();
        visit1.setPet(p1);
        visit1.setDate(LocalDate.now());
        visit1.setDescription("asdfasdfa");
        visitService.save(visit1);
        System.out.println("Loaded Vets");
        System.out.println("Loaded Visits");

    }
}
